package de.grunicke.gongapp.rest;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import de.grunicke.gongapp.GongActivity;

/**
 * Created by Børge on 05.06.2015.
 */
public class RestService extends AsyncTask{
    private final static String urlString = "http://192.168.2.63:8080/gong-rest";
    String data = "type=test+type&message= HALLOO WELT";

    @Override
    protected Object doInBackground(Object[] params) {
        System.out.println("starting async task");
        try {
            URL url = new URL(urlString);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
            outputStreamWriter.write(data);
            outputStreamWriter.flush();
            outputStreamWriter.close();
            String interest = httpURLConnection.getResponseMessage();
            boolean cache = httpURLConnection.getUseCaches();
            String temp = httpURLConnection.getHeaderField(6);
            System.out.println("permission: " + httpURLConnection.getPermission().getName());
            System.out.println(interest + ", cache: " + cache);
            System.out.println(temp);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
